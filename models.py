# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class GeospatialLocation(models.Model):
    id = models.BigAutoField(primary_key=True)
    address = models.CharField(max_length=100)
    point = models.PointField()
    polygons = models.MultiPolygonField()
    lines = models.MultiLineStringField()

    class Meta:
        managed = False
        db_table = 'geospatial_location'


class RefContractor(models.Model):
    id = models.BigAutoField(primary_key=True)
    name_kz = models.TextField(blank=True, null=True)
    name_ru = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    supervisor = models.TextField(blank=True, null=True)
    old_name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ref_contractor'


class RefDepartment(models.Model):
    id = models.BigAutoField(primary_key=True)
    name_kz = models.TextField(blank=True, null=True)
    name_ru = models.TextField(blank=True, null=True)
    short_name_kz = models.CharField(max_length=255, blank=True, null=True)
    short_name_ru = models.CharField(max_length=255, blank=True, null=True)
    supervisor_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ref_department'


class RefGeoStatus(models.Model):
    id = models.BigAutoField(primary_key=True)
    color = models.CharField(max_length=255, blank=True, null=True)
    name_kz = models.CharField(max_length=255, blank=True, null=True)
    name_ru = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ref_geo_status'


class RepairWorkFiles(models.Model):
    id = models.BigAutoField(primary_key=True)
    category = models.CharField(max_length=255, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    path = models.TextField(blank=True, null=True)
    pid = models.BigIntegerField(blank=True, null=True)
    date_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'repair_work_files'


class RepairWorks(models.Model):
    id = models.BigAutoField(primary_key=True)
    address = models.TextField(blank=True, null=True)
    linestring = models.LineStringField(blank=True, null=True)
    marker = models.PointField(blank=True, null=True)
    multipolygon = models.MultiPolygonField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    warranty = models.TextField(blank=True, null=True)
    is_blocked = models.BooleanField(blank=True, null=True)
    work_end_time = models.TimeField(blank=True, null=True)
    work_start_time = models.TimeField(blank=True, null=True)
    contractor = models.ForeignKey(RefContractor, models.DO_NOTHING, blank=True, null=True)
    department = models.ForeignKey(RefDepartment, models.DO_NOTHING, blank=True, null=True)
    status = models.ForeignKey(RefGeoStatus, models.DO_NOTHING, blank=True, null=True)
    passport = models.ForeignKey(RepairWorkFiles, models.DO_NOTHING, blank=True, null=True)
    preview = models.ForeignKey(RepairWorkFiles, models.DO_NOTHING, related_name='repairworks_preview_set', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'repair_works'
