from rest_framework import serializers
from .models import RefContractor, RefDepartment, RefGeoStatus, RepairWorkFiles, RepairWorks, RefCategory, RefCategorySubs


class RefCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = RefCategory
        fields = '__all__'


class RefCategorySubsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefCategorySubs
        fields = '__all__'


class RefContractorSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefContractor
        fields = '__all__'


class RefDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefDepartment
        fields = '__all__'


class RefGeoStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefGeoStatus
        fields = '__all__'


class RepairWorkFilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = RepairWorkFiles
        fields = '__all__'


class RepairWorksSerializer(serializers.ModelSerializer):
    class Meta:
        model = RepairWorks
        fields = '__all__'
