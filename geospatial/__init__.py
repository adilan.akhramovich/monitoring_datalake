from .apps import GeospatialConfig as default_app_config


default_app_config = 'geospatial.default_app_config'
