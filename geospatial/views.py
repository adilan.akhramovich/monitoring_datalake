from rest_framework import viewsets
from .models import RefContractor, RefDepartment, RefGeoStatus, RepairWorkFiles, RepairWorks, RefCategory, RefCategorySubs
from .serializers import RefContractorSerializer, RefDepartmentSerializer, RefGeoStatusSerializer, RepairWorkFilesSerializer, RepairWorksSerializer, RefCategorySerializer, RefCategorySubsSerializer


class RefCategoryViewSet(viewsets.ModelViewSet):
    queryset = RefCategory.objects.all()
    serializer_class = RefCategorySerializer


class RefCategorySubsViewSet(viewsets.ModelViewSet):
    queryset = RefCategorySubs.objects.all()
    serializer_class = RefCategorySubsSerializer


class RefContractorViewSet(viewsets.ModelViewSet):
    queryset = RefContractor.objects.all()
    serializer_class = RefContractorSerializer


class RefDepartmentViewSet(viewsets.ModelViewSet):
    queryset = RefDepartment.objects.all()
    serializer_class = RefDepartmentSerializer


class RefGeoStatusViewSet(viewsets.ModelViewSet):
    queryset = RefGeoStatus.objects.all()
    serializer_class = RefGeoStatusSerializer


class RepairWorkFilesViewSet(viewsets.ModelViewSet):
    queryset = RepairWorkFiles.objects.all()
    serializer_class = RepairWorkFilesSerializer


class RepairWorksViewSet(viewsets.ModelViewSet):
    queryset = RepairWorks.objects.all()
    serializer_class = RepairWorksSerializer
