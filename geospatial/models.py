from __future__ import unicode_literals
import os
from functools import partial
from django.db import models
from django.contrib.gis.db import models
from django.utils import timezone


class RefCategory(models.Model):
    id = models.BigAutoField(primary_key=True)
    name_kz = models.TextField(verbose_name="Название на казахском")
    name_ru = models.TextField(verbose_name="Название на русском")
    desc_name_kz = models.TextField(verbose_name="Описание на казахском", null=True)
    desc_name_ru = models.TextField(verbose_name="Описание на русском", null=True)

    def __str__(self):
        return self.name_ru

    class Meta:
        db_table = 'ref_category'
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        # Add any other options or constraints you need


class RefCategorySubs(models.Model):
    id = models.BigAutoField(primary_key=True)
    icon = models.TextField(verbose_name="Иконка", null=True)
    image = models.TextField(verbose_name="Картинка", null=True)
    cat_id = models.ForeignKey(RefCategory, on_delete=models.CASCADE, related_name='category_subs', verbose_name="Категория")
    sub_cat_id = models.ForeignKey(RefCategory, on_delete=models.CASCADE, related_name='sub_category_subs', verbose_name="Под категория")
    category_old = models.TextField(verbose_name="Название категории")
    
    def __str__(self):
        return self.category_old

    class Meta:
        db_table = 'ref_category_subs'
        verbose_name = "Под категория"
        verbose_name_plural = "Под категории"
        # Add any other options or constraints you need


class RefContractor(models.Model):
    id = models.BigAutoField(primary_key=True)
    name_kz = models.TextField(blank=True, null=True, verbose_name="Имя на казахском")
    name_ru = models.TextField(blank=True, null=True, verbose_name="Имя на русском")
    phone = models.TextField(blank=True, null=True, verbose_name="Телефон")
    supervisor = models.TextField(blank=True, null=True, verbose_name="Ответственный")
    old_name = models.TextField(blank=True, null=True, verbose_name="Имя старое")

    def __str__(self):
        return self.name_ru

    class Meta:
        db_table = 'ref_contractor'
        verbose_name = "Застройщик"
        verbose_name_plural = "Застройщики"


class RefDepartment(models.Model):
    id = models.BigAutoField(primary_key=True)
    name_kz = models.TextField(blank=True, null=True, verbose_name="Название на казахском")
    name_ru = models.TextField(blank=True, null=True, verbose_name="Название на русском")
    short_name_kz = models.CharField(max_length=255, blank=True, null=True, verbose_name="Короткое название на казахском")
    short_name_ru = models.CharField(max_length=255, blank=True, null=True, verbose_name="Короткое название на русском")
    supervisor_id = models.BigIntegerField(blank=True, null=True, verbose_name="ID ответственного")

    def __str__(self):
        return self.name_ru

    class Meta:
        db_table = 'ref_department'
        verbose_name = "Ответственный орган"
        verbose_name_plural = "Ответственные органы"


class RefGeoStatus(models.Model):
    id = models.BigAutoField(primary_key=True)
    color = models.CharField(max_length=255, blank=True, null=True, verbose_name="Цвет")
    name_kz = models.CharField(max_length=255, blank=True, null=True, verbose_name="Название на казахском")
    name_ru = models.CharField(max_length=255, blank=True, null=True, verbose_name="Название на русском")

    def __str__(self):
        return self.name_ru

    class Meta:
        db_table = 'ref_geo_status'
        verbose_name = "Статус работы"
        verbose_name_plural = "Статусы работ"


def repair_work_file_path(directory, instance, filename):
    # Get the current timestamp in the format YYYYMMDD_HHMMSS
    timestamp = timezone.now().strftime('%Y%m%d_%H%M%S')

    # Extract the file extension from the original filename
    file_extension = os.path.splitext(filename)[1]

    # Preserve the original filename (without extension) to include it in the new filename
    original_filename = os.path.splitext(os.path.basename(filename))[0]

    # Combine the timestamp, original filename, and file extension to form the new filename
    new_filename = f'{directory}/{timestamp}_{original_filename}{file_extension}'

    return new_filename


class RepairWorkFiles(models.Model):
    id = models.BigAutoField(primary_key=True)
    category = models.CharField(max_length=255, blank=True, null=True, verbose_name="Категория")
    name = models.TextField(blank=True, null=True, verbose_name="Название")
    type = models.CharField(max_length=255, blank=True, null=True, verbose_name="Тип")
    path = models.TextField(blank=True, null=True, verbose_name="Путь до файла")
    pid = models.BigIntegerField(blank=True, null=True, verbose_name="ID паспорта")
    date_time = models.DateTimeField(blank=True, null=True, verbose_name="Время и дата")

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'repair_work_files'
        verbose_name = "Файл"
        verbose_name_plural = "Файлы"


class RepairWorks(models.Model):
    id = models.BigAutoField(primary_key=True)
    category = models.ForeignKey(RefCategory, models.DO_NOTHING, related_name='category_sub', verbose_name="Категория", default=None)
    sub_category = models.ForeignKey(RefCategorySubs, models.DO_NOTHING, related_name='sub_category_sub', verbose_name="Подкатегория", default=None)
    address = models.TextField(verbose_name="Адрес")
    linestring = models.LineStringField(verbose_name="Линия координат")
    marker = models.PointField(verbose_name="Точка координат")
    multipolygon = models.MultiPolygonField(verbose_name="Полигоны координат")
    description = models.TextField(verbose_name="Описание", blank=True, null=True)
    end_date = models.DateField(verbose_name="День начала")
    start_date = models.DateField(verbose_name="День окончания")
    warranty = models.TextField(verbose_name="Гарантия", blank=True, null=True)
    is_blocked = models.BooleanField(verbose_name="Перекрытие дорог")
    work_end_time = models.TimeField(verbose_name="Время окончания работы", blank=True, null=True)
    work_start_time = models.TimeField(verbose_name="Время начала работы", blank=True, null=True)
    contractor = models.ForeignKey(RefContractor, models.DO_NOTHING, verbose_name="Застройщик", blank=True, null=True)
    department = models.ForeignKey(RefDepartment, models.DO_NOTHING, verbose_name="Ответственный орган")
    status = models.ForeignKey(RefGeoStatus, models.DO_NOTHING, verbose_name="Статус")
    document = models.FileField(
        upload_to=partial(repair_work_file_path, 'repair_work_documents'),
        verbose_name="Пасспорт обьекта", blank=True, null=True
    )
    photo = models.ImageField(
        upload_to=partial(repair_work_file_path, 'repair_works_photos'),
        verbose_name="Фото", blank=True, null=True
    )


    def __str__(self):
        return self.address

    class Meta:
        db_table = 'repair_works'
        verbose_name = "Ремонтные работы"
        verbose_name_plural = "Ремонтные работы"