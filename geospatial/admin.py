from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from django.contrib.gis.db import models as geo_models
from leaflet.forms.widgets import LeafletWidget
from django.utils.html import format_html
from .models import RepairWorks, RefDepartment, RefContractor, RefGeoStatus, RepairWorkFiles, RefCategory, RefCategorySubs


admin.site.site_header  =  "Мониторинг районов"  
admin.site.site_title  =  "Мониторинг районов"
admin.site.index_title  =  "Мониторинг районов"


LEAFLET_WIDGET_ATTRS = {
    'map_height': '500px',
    'map_width': '100%',
    'display_raw': 'true',
    'map_srid': 4326,
}

LEAFLET_FIELD_OPTIONS = {'widget': LeafletWidget(attrs=LEAFLET_WIDGET_ATTRS)}


class RefCategoryAdmin(LeafletGeoAdmin):
    list_display = ('id', 'name_kz', 'name_ru', 'desc_name_kz', 'desc_name_ru')  # Add the fields you want to list
    search_fields = ['name_kz', 'name_ru', 'desc_name_kz', 'desc_name_ru']  # Add the fields you want to search on
    ordering = ('id',)  # Add the fields you want to sort on
admin.site.register(RefCategory, RefCategoryAdmin)


class RefCategorySubsAdmin(LeafletGeoAdmin):
    list_display = ('id', 'icon', 'image', 'cat_id', 'sub_cat_id', 'category_old')  # Add the fields you want to list
    search_fields = ['icon', 'image', 'cat_id', 'sub_cat_id', 'category_old']  # Add the fields you want to search on
    ordering = ('id',)  # Add the fields you want to sort on
admin.site.register(RefCategorySubs, RefCategorySubsAdmin)


class RepairWorksAdmin(LeafletGeoAdmin):
    formfield_overrides = {
        geo_models.PointField: LEAFLET_FIELD_OPTIONS,
        geo_models.MultiPointField: LEAFLET_FIELD_OPTIONS,
        geo_models.LineStringField: LEAFLET_FIELD_OPTIONS,
        geo_models.MultiLineStringField: LEAFLET_FIELD_OPTIONS,
        geo_models.PolygonField: LEAFLET_FIELD_OPTIONS,
        geo_models.MultiPolygonField: LEAFLET_FIELD_OPTIONS,
    }
    def photo_link(self, obj):
        # Generate the URL for the photo
        photo_url = obj.photo.url
        # Generate the link with target="_blank" attribute
        link = format_html('<a href="{}" target="_blank">View Photo</a>', photo_url)
        return link
    photo_link.allow_tags = True
    photo_link.short_description = "Photo"  # This will be the column header in the list view

    list_display = ('id', 'address', 'linestring', 'marker', 'multipolygon', 'description', 'end_date', 'start_date', 'warranty', 'is_blocked', 'work_end_time', 'work_start_time', 'contractor', 'department', 'status', 'document', 'photo' )  # Add the fields you want to list
    search_fields = ['address', 'linestring', 'marker', 'multipolygon', 'description', 'end_date', 'start_date', 'warranty', 'is_blocked', 'work_end_time', 'work_start_time', 'contractor', 'department', 'status', 'document', 'photo']  # Add the fields you want to search on
    ordering = ('address',)  # Add the fields you want to sort on

admin.site.register(RepairWorks, RepairWorksAdmin)


class RefDepartmentAdmin(LeafletGeoAdmin):
    list_display = ('id', 'name_kz', 'name_ru', 'short_name_kz', 'short_name_ru', 'supervisor_id')  # Add the fields you want to list
    search_fields = ['name_kz', 'name_ru', 'short_name_kz', 'short_name_ru']  # Add the fields you want to search on
    ordering = ('name_ru',)  # Add the fields you want to sort on

admin.site.register(RefDepartment, RefDepartmentAdmin)


class RefContractorAdmin(LeafletGeoAdmin):
    list_display = ('name_kz', 'name_ru', 'phone', 'supervisor', 'old_name') # Add the fields you want to list
    search_fields = ['name_kz', 'name_ru', 'phone', 'supervisor', 'old_name'] # Add the fields you want to search on
    ordering = ('name_ru',)  # Add the fields you want to sort on
    
admin.site.register(RefContractor, RefContractorAdmin)


class RefGeoStatusAdmin(LeafletGeoAdmin):
    list_display = ('id', 'color', 'name_kz', 'name_ru')  # Add the fields you want to list
    search_fields = ['color', 'name_kz', 'name_ru']  # Add the fields you want to search on
    ordering = ('name_ru',)  # Add the fields you want to sort on
    
admin.site.register(RefGeoStatus, RefGeoStatusAdmin)


class RepairWorkFilesAdmin(LeafletGeoAdmin):
    list_display = ('id', 'category', 'name', 'type', 'path', 'pid', 'date_time')  # Add the fields you want to list
    search_fields = ['category', 'name', 'type', 'path', 'pid', 'date_time']  # Add the fields you want to search on
    ordering = ('category',)  # Add the fields you want to sort on

admin.site.register(RepairWorkFiles, RepairWorkFilesAdmin)