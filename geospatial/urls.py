from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('ref_category', views.RefCategoryViewSet)
router.register('ref_sub_category', views.RefCategorySubsViewSet)
router.register('ref_contractors', views.RefContractorViewSet)
router.register('ref_departments', views.RefDepartmentViewSet)
router.register('ref_geo_statuses', views.RefGeoStatusViewSet)
router.register('repair_work_files', views.RepairWorkFilesViewSet)
router.register('repair_works', views.RepairWorksViewSet)

urlpatterns = [
    path('', include(router.urls)),
    # Other URL patterns...
]
