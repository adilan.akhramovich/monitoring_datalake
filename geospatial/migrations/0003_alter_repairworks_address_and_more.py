# Generated by Django 4.2.3 on 2023-07-21 10:21

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import functools
import geospatial.models


class Migration(migrations.Migration):

    dependencies = [
        ('geospatial', '0002_alter_repairworks_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repairworks',
            name='address',
            field=models.TextField(verbose_name='Адрес'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='contractor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='geospatial.refcontractor', verbose_name='Застройщик'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='department',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='geospatial.refdepartment', verbose_name='Ответственный орган'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='description',
            field=models.TextField(verbose_name='Описание'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='document',
            field=models.FileField(upload_to=functools.partial(geospatial.models.repair_work_file_path, *('repair_work_documents',), **{}), verbose_name='Документ, Файл'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='end_date',
            field=models.DateField(verbose_name='День начала'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='is_blocked',
            field=models.BooleanField(verbose_name='Перекрытие дорог'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='linestring',
            field=django.contrib.gis.db.models.fields.LineStringField(srid=4326, verbose_name='Линия координат'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='marker',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, verbose_name='Точка координат'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='multipolygon',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326, verbose_name='Полигоны координат'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='photo',
            field=models.ImageField(upload_to=functools.partial(geospatial.models.repair_work_file_path, *('repair_works_photos',), **{}), verbose_name='Фото'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='start_date',
            field=models.DateField(verbose_name='День окончания'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='geospatial.refgeostatus', verbose_name='Статус'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='warranty',
            field=models.TextField(verbose_name='Гарантия'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='work_end_time',
            field=models.TimeField(verbose_name='Время окончания работы'),
        ),
        migrations.AlterField(
            model_name='repairworks',
            name='work_start_time',
            field=models.TimeField(verbose_name='Время начала работы'),
        ),
    ]
